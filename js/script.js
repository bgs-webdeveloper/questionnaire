(function($) {
$(function() {
    var selectedElementInStock;
    $('input, select.select-style').styler();
    /* $('.jq-selectbox').on('click', function(e){
        e.preventDefault();
        var dropdown = $(this).find('.jq-selectbox__dropdown');

        if($(this).hasClass('opened')){
            console.log('Opened');
            
            return
        }

        if(dropdown.is(':hidden')){
            console.log('Hidden');
            dropdown.show();
        }
        else{
            console.log("Show");
            dropdown.hide();
        }
    }); */
    /* $('.jq-selectbox__dropdown').on('click', function(){
        console.log('Click');
        $(this).hide();
    }); */
    $('.required').on('input', function(){
        var valueLength = $(this).val().length;

        if(valueLength > 0){
            if($(this).hasClass('validation-phone-number')){
                var reg = /^\+\d+$/;
                var value = $(this).val();


                if(value[0] == '+'){
                    
                    if(value[1] && value[1] != '7') {
                        console.log(value[1]);
                        $(this).val(value.substring(0, value.length - 1));
                    };
                    if(value.length > 1 && !value.match(reg)){
                        $(this).val(value.substring(0, value.length - 1));
                    }
                }   
                else {
                    $(this).val(value.substring(0, value.length - 1)); 
                }
                if(value.length == 10){
                    $(this).removeClass('not-valid');
                } 
                else{
                    if(value.length > 10) $(this).val(value.substring(0, value.length - 1)); 
                }
            }
            else{
                $(this).removeClass('not-valid');
            }
        }
        else{
            $(this).addClass('not-valid');
        }
    });
    $('.js_go-to-step2').on('click', function(e){
        e.preventDefault();

        var notValidLength = $(this).closest('form').find('.not-valid').length;

        if(notValidLength === 0){
            $('.js_user-data-screen').addClass('hidden');
            $('.js_stock-slider-screen').removeClass('hidden');
            initSlider();
        }
        else{
            $('.required').each(function(i, item){
                if($(item).hasClass('not-valid')){
                    $(item).css('border-color','#ed006a');
                }
                else{
                    $(item).css('border-color','#eee');
                }
            });
        }
    });

    $('.js_form-check-for-input-disabled').on('click', function(){
        if($('.input-checkbox').hasClass('checked')){
            console.log('true');
            $(this).find('.form-field').prop("disabled", false);
        }
        else{
            console.log('false');
            $(this).find('.form-field').prop("disabled", true);
        }
    });
    /* $('.js_stock-slider-screen .stock-item-inner').on('click', function(){
        var $this = $(this);

        if ($this.parent('.stock-item--news').length > 0) return;

        if($this.hasClass('active')){
            $(this).removeClass('active');
        }
        else{
            $(this).addClass('active');
        }
    }); */

    function initSlider (){
        // Swiper slider
        var mySwiper = new Swiper ('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            draggable: false,
            simulateTouch: false,
            navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
            }
        });
    }

    $('.js_init-select-style').on('click', function(){
        
        selectedElementInStock = $(this);

        setTimeout(function(){
            $('select').styler();
            console.log('Init end');
        }, 300);
    });

    $('.js_select-stock-item').on('click', function(){
        selectedElementInStock.find('.stock-item-inner').addClass('active');
    });

    $('.js_go-to-step3').on('click', function(){
        $('.js_stock-slider-screen').addClass('hidden');
        $('.js_request-done').removeClass('hidden');
    });

    

});

})(jQuery);